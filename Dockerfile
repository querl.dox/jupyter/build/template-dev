FROM python:alpine

RUN apk add --no-cache cython linux-headers alpine-sdk build-base nodejs npm
RUN pip install pyyaml toml xmltodict jinja2 jinja2-cli[yaml] jinja2-cli[toml] jinja2-cli[xml] jinja-cli
RUN npm install hbs-cli

# install the notebook package
RUN pip install pyzmq --install-option="--zmq=bundled" && \
    pip install notebook
